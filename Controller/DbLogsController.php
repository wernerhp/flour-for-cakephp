<?php

class DbLogsController extends FlourAppController {

    var $name = 'DbLogs';
    var $paginate = array(
        'limit' => 10,
        'order' => 'time desc,  id desc'
    );

    function index() {
        $this->DbLog->recursive = 0;
        $this->set('dbLogs', $this->paginate());
    }

    function view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid db log'));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('dbLog', $this->DbLog->read(null, $id));
    }

    function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for db log'));
            $this->redirect(array('action' => 'index'));
        }
        if ($this->DbLog->delete($id)) {
            $this->Session->setFlash(__('Db log deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Db log was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

    function truncate() {
        $dbo = $this->DbLog->getDataSource();

        if ($this->DbLog->query('TRUNCATE ' . $dbo->fullTableName($this->DbLog))) {
            $this->Session->setFlash(__('Database Logs cleared.'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Database Logs was not deleted'));
        $this->redirect(array('action' => 'index'));
    }

}

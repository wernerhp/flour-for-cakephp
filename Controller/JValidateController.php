<?php

class JValidateController extends FlourAppController {

    public function beforeFilter() {
        parent::beforeFilter();

        if (!empty($this->Auth)) {
            $this->Auth->allowedActions = array('jValidate');
        }
    }

    public function jvalidate() {

        if ($this->request->is('ajax')) {
            if (!empty($this->request->data)) {
                $models = array_keys($this->request->data);
                $model = $models[0];

                $fields = array_keys($this->request->data[$model]);
                $field = $fields[0];

                $this->loadModel($model);
                $this->$model->set($this->request->data);

                if ($this->$model->validates(array('fieldList' => array($field)))) {
                    $this->set('validationErrors', '');
                } else {
                    $validationErrors = $this->validateErrors($this->$model);
                    $this->set('validationErrors', $validationErrors[$field]);
                }
                $this->set('_serialize', 'validationErrors');
            } else {
                throw new BadRequestException();
            }
        }
    }
}

?>

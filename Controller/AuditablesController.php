<?php

class AuditablesController extends FlourAppController {

    var $name = 'Auditables';
    var $uses = array('Revision');
    var $paginate = array(
        'limit' => 10,
        'order' => 'time desc,  id desc'
    );

    function admin_history($id = null) {
        $this->loadModel('Revision');
        $this->Revision->recursive = 1;
        $this->paginate = array(
            'Revision' => array(
                'conditions' => array(
                    'Revision.model' => 'Product',
                    'Revision.foreign_key' => $id
                ),
                'order' => array('Revision.created' => 'desc')
            )
        );
        $revisions = $this->paginate('Revision');
        $this->set('revisions', $revisions);
    }

}

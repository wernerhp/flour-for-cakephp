<?php

/*
 *
 * Using the Schema command line utility
 * cake schema create Flour.DbLog
 * NOTE: Diable plugin's bootstrap before running this
 */

class DbLogSchema extends CakeSchema {

    var $name = 'DbLog';

    function before($event = array()) {
        return true;
    }

    function after($event = array()) {
        
    }

    /*
      CREATE TABLE  `db_logs` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
      `time` datetime DEFAULT NULL,
      `message` text COLLATE utf8_unicode_ci,
      PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
     */

    var $db_logs = array(
        'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
        'type' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 20),
        'time' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'message' => array('type' => 'text', 'null' => true, 'default' => NULL),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
        'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
    );

}

<?php

/*
 *
 * Using the Schema command line utility
 * cake schema create Flour.Auditable
 */

class AuditableSchema extends CakeSchema {

    var $name = 'Auditable';

    function before($event = array()) {
        return true;
    }

    function after($event = array()) {
        
    }

    /*
    DROP TABLE IF EXISTS `revisions`;
      CREATE  TABLE IF NOT EXISTS `revisions` (
      `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
      `user_id` INT UNSIGNED NOT NULL ,
      `event` VARCHAR(255) NOT NULL ,
      `model` VARCHAR(255) NOT NULL ,
      `foreign_key` INT UNSIGNED NOT NULL ,
      `created` DATETIME NULL ,
      `modified` TIMESTAMP NULL ,
      PRIMARY KEY (`id`) ,
      INDEX `revisions_users_user_id` (`user_id` ASC) ,
      CONSTRAINT `revisions_users_user_id`
      FOREIGN KEY (`user_id` )
      REFERENCES `users` (`id` )
      ON DELETE NO ACTION
      ON UPDATE NO ACTION)
      ENGINE = InnoDB;

DROP TABLE IF EXISTS `fields`;
      CREATE  TABLE IF NOT EXISTS `fields` (
      `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
      `revision_id` INT UNSIGNED NOT NULL ,
      `name` VARCHAR(255) NOT NULL ,
      `old_value` TEXT NULL ,
      `new_value` TEXT NULL ,
      `created` DATETIME NULL ,
      `modified` TIMESTAMP NULL ,
      PRIMARY KEY (`id`) ,
      INDEX `fields_revisions_revision_id` (`revision_id` ASC) ,
      CONSTRAINT `fields_revisions_revision_id`
      FOREIGN KEY (`revision_id` )
      REFERENCES `revisions` (`id` )
      ON DELETE NO ACTION
      ON UPDATE NO ACTION)
      ENGINE = InnoDB;
     */

    var $revisions = array(
        'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 11, 'key' => 'primary'),
        'otu' => array('type' => 'string', 'null' => false, 'default' => NULL),
        'otp' => array('type' => 'string', 'null' => false, 'default' => NULL),
        'action' => array('type' => 'string', 'null' => true, 'default' => NULL),
        'attempts_allowed' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 4),
        'attempts' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 4),
        'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
        'modified' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
        'expires' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
        'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
    );

}

<?php

/*
 *
 * Using the Schema command line utility
 * cake schema create Flour.Otp
 */

class OtpSchema extends CakeSchema {

    var $name = 'Otp';

    function before($event = array()) {
        return true;
    }

    function after($event = array()) {
        
    }

    /*
      DROP TABLE IF EXISTS `flour`.`otps`;
      CREATE TABLE  `flour`.`otps` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `otu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
      `otp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
      `action` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
      `attempts_allowed` int(4) NOT NULL,
      `attempts` int(4) NOT NULL,
      `created` datetime NOT NULL,
      `modified` timestamp NOT NULL,
      `expires` datetime NOT NULL,
      PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
     */

    var $otps = array(
        'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 11, 'key' => 'primary'),
        'otu' => array('type' => 'string', 'null' => false, 'default' => NULL),
        'otp' => array('type' => 'string', 'null' => false, 'default' => NULL),
        'action' => array('type' => 'string', 'null' => true, 'default' => NULL),
        'attempts_allowed' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 4),
        'attempts' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 4),
        'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
        'modified' => array('type' => 'timestamp', 'null' => false, 'default' => NULL),
        'expires' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
        'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
    );

}

<?php

/*
 *
 * Using the Schema command line utility
 * cake schema create Flour.Clusterable
 * NOTE: Diable plugin's bootstrap before running this
 */

class ClusterableSchema extends CakeSchema {

    var $name = 'Clusterable';

    function before($event = array()) {
        return true;
    }

    function after($event = array()) {
        
    }

    /*
      CREATE TABLE  `clusters` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `model` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
      `zoom_level` int(11) NOT NULL,
      `size` int(11) NOT NULL DEFAULT '0',
      `longitude` float NOT NULL,
      `latitude` float NOT NULL,
      `created` datetime NOT NULL,
      `modified` timestamp NOT NULL,
      PRIMARY KEY (`id`),
      KEY `clusters_longitude` (`longitude`),
      KEY `clusters_model` (`model`),
      KEY `clusters_zoom_level` (`zoom_level`),
      KEY `clusters_latitude` (`latitude`),
      KEY `clusters_size` (`size`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
     */

    var $clusters = array(
        'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
        'model' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 45, 'key' => 'index'),
        'zoom_level' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'index'),
        'size' => array('type' => 'integer', 'null' => false, 'default' => '0', 'key' => 'index'),
        'longitude' => array('type' => 'float', 'null' => false, 'default' => NULL, 'key' => 'index'),
        'latitude' => array('type' => 'float', 'null' => false, 'default' => NULL, 'key' => 'index'),
        'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
        'modified' => array('type' => 'timestamp', 'null' => false, 'default' => NULL),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'clusters_longitude' => array('column' => 'longitude', 'unique' => 0), 'clusters_model' => array('column' => 'model', 'unique' => 0), 'clusters_zoom_level' => array('column' => 'zoom_level', 'unique' => 0), 'clusters_latitude' => array('column' => 'latitude', 'unique' => 0), 'clusters_size' => array('column' => 'size', 'unique' => 0)),
        'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
    );

    /*
      CREATE TABLE  `clusters_points` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `cluster_id` int(11) NOT NULL,
      `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
      `foreign_key` int(11) NOT NULL,
      `created` datetime NOT NULL,
      `modified` timestamp NOT NULL,
      `zoom_level` int(11) NOT NULL,
      PRIMARY KEY (`id`),
      KEY `clusters_points_cluster_id` (`cluster_id`),
      KEY `clusters_points_foreign_key` (`foreign_key`),
      KEY `clusters_points_model` (`model`),
      KEY `clusters_points_zoom_level` (`zoom_level`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
     */
    var $clusters_points = array(
        'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
        'cluster_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'index'),
        'model' => array('type' => 'string', 'null' => false, 'default' => NULL, 'key' => 'index'),
        'foreign_key' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'index'),
        'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
        'modified' => array('type' => 'timestamp', 'null' => false, 'default' => NULL),
        'zoom_level' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'index'),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'clusters_points_cluster_id' => array('column' => 'cluster_id', 'unique' => 0), 'clusters_points_foreign_key' => array('column' => 'foreign_key', 'unique' => 0), 'clusters_points_model' => array('column' => 'model', 'unique' => 0), 'clusters_points_zoom_level' => array('column' => 'zoom_level', 'unique' => 0)),
        'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
    );

}

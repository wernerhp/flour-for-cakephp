<?php

/* Flour schema generated on: 2011-09-27 23:43:55 : 1317156235 */

class FlourSchema extends CakeSchema {

    var $name = 'Flour';

    function before($event = array()) {
        return true;
    }

    function after($event = array()) {
        
    }

    var $clusters = array(
        'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
        'model' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 45, 'key' => 'index', 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
        'zoom_level' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'index'),
        'size' => array('type' => 'integer', 'null' => false, 'default' => '0', 'key' => 'index'),
        'longitude' => array('type' => 'float', 'null' => false, 'default' => NULL, 'key' => 'index'),
        'latitude' => array('type' => 'float', 'null' => false, 'default' => NULL, 'key' => 'index'),
        'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
        'modified' => array('type' => 'timestamp', 'null' => false, 'default' => NULL),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'clusters_longitude' => array('column' => 'longitude', 'unique' => 0), 'clusters_model' => array('column' => 'model', 'unique' => 0), 'clusters_zoom_level' => array('column' => 'zoom_level', 'unique' => 0), 'clusters_latitude' => array('column' => 'latitude', 'unique' => 0), 'clusters_size' => array('column' => 'size', 'unique' => 0)),
        'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
    );
    var $clusters_points = array(
        'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
        'cluster_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'index'),
        'model' => array('type' => 'string', 'null' => false, 'default' => NULL, 'key' => 'index', 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
        'foreign_key' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'index'),
        'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
        'modified' => array('type' => 'timestamp', 'null' => false, 'default' => NULL),
        'zoom_level' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'index'),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'clusters_points_cluster_id' => array('column' => 'cluster_id', 'unique' => 0), 'clusters_points_foreign_key' => array('column' => 'foreign_key', 'unique' => 0), 'clusters_points_model' => array('column' => 'model', 'unique' => 0), 'clusters_points_zoom_level' => array('column' => 'zoom_level', 'unique' => 0)),
        'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
    );
    var $db_logs = array(
        'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
        'type' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 20, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
        'time' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
        'message' => array('type' => 'text', 'null' => true, 'default' => NULL, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
        'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
    );
    var $otps = array(
        'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
        'otu' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
        'otp' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
        'action' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
        'attempts_allowed' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 4),
        'attempts' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 4),
        'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
        'modified' => array('type' => 'timestamp', 'null' => false, 'default' => NULL),
        'expires' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
        'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
        'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
    );

}

?>
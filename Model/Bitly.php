<?php

App::uses('AppModel', 'Model');
App::uses('HttpSocket', 'Network/Http');
App::uses('ConnectionManager', 'Model');

/**
 * Bitly Model
 *
 * A CakePHP wrapper for the Bit.ly API.
 * API Documentation http://dev.bitly.com/links.html
 *
 * @author  Werner Pieterson <wernerhp@gmail.com>
 * 
 * Examples:
 *    database.php
 *    public $test = array(
 *          'datasource' => 'Flour.ArraySource',
 *          'host' => 'https://api-ssl.bitly.com',
 *          'login' => '**login**',
 *          'api_key' => '**apiKey**',
 *          'logging' => true,  // true | false
 *      );
 *    
 *    $this->loadModel('Flour.Bitly');
 *    debug($this->Bitly->shorten(array('uri'=>'http://www.google.com/')));
 *    debug($this->Bitly->expand(array('shortUrl'=>'http://bit.ly/IjjSXI', 'hash'=>'IjjSXI')));
 */
class Bitly extends FlourAppModel {

    public $useTable = false;
    private $host = '';
    private $login = '';
    private $apiKey = '';
    private $httpSocket = '';

    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);

        // Gets the Bit.ly API login and key from database.php
        $dataSource = ConnectionManager::getDataSource('bitly');
        $this->host = $dataSource->config['host'] . '/v3/';
        $this->login = $dataSource->config['login'];
        $this->apiKey = $dataSource->config['api_key'];
        $this->logging = empty($dataSource->config['logging']) ? false : $dataSource->config['logging'];
        $this->httpSocket = new HttpSocket();
    }

    /**
     * This magic method builds and makes an HttpSocekt request to the Bit.ly API
     * @param type $method The Bit.ly API method being called
     * @param type $params Query parameters to be passed
     * @return type
     */
    public function __call($method, $params) {

        $query = array(
            'login' => $this->login,
            'apikey' => $this->apiKey,
            'format' => 'json',
        );
        $query = array_merge($query, $params[0]);
        $socketResponse = $this->httpSocket->get($this->host . $method, $query);
        $response = json_decode($socketResponse->body);
        $response = Set::reverse($response);

        // Logs Bitly request and response to a file
        if ($this->logging) {
            $log = array();

            $log['SOCKET'] = array(
                'REQUEST' => $this->httpSocket->request,
                'RESPONSE' => $this->httpSocket->response,
            );
            $this->log($log, 'bitly');
        }

        if ($this->httpSocket->response->code == 0) {
            throw new InternalErrorException(String::insert(__('Unable to connect to :host.'), array('host' => $this->host)));
        }

        if ($response['status_code'] != 200) {
            trigger_error(__d('cake_dev', $response['status_code'] . ' ' . $response['status_txt']), E_USER_NOTICE);
        }

        return $response['data'];
    }
}

?>

<?php

/**
 * Behavior to keep an audit trail of any modifications performed on a model's data
 */
class AuditableBehavior extends ModelBehavior {

    /**
     * Initiate behavior for the model using specified settings.
     *
     * @param object $Model Model using the behavior
     * @param array $settings Settings to override for model
     * @access public
     */
    function setup(&$Model, $settings = array()) {
        if (!isset($this->settings[$Model->alias])) {
            $this->settings[$Model->alias] = array('user' => 'Auth.User');
        }
        if (!is_array($settings)) {
            $settings = array();
        }
        $this->settings[$Model->alias] = array_merge(
                $this->settings[$Model->alias], $settings
        );
    }

    /**
     * Called before the model's beforeSave
     * 
     * @param object $Model The current model
     */
    function beforeSave(&$Model) {
        parent::beforeSave($Model);
        if (isset($Model->data[$Model->alias]['id'])) {
            $id = $Model->data[$Model->alias]['id'];
            $this->_data = $Model->findById($id);
        }
    }

    /**
     * Called before the model's afterSave
     * 
     * @param object $Model The current model
     * @param mixed $created true when a record is created, and false when a record is updated 
     */
    function afterSave(&$Model, $created) {
        parent::afterSave($Model, $created);

        App::import('Component', 'Session');
        $Session = new SessionComponent();
        $user = $Session->read($this->settings[$Model->alias]['user']);

        App::import('Model', 'Revision');
        $Revision = new Revision();
        $data = array(
            'Revision' => array(
                'user_id' => $user['id'],
                'model' => $Model->alias,
                'foreign_key' => $Model->id
            )
        );

        if ($created) {
            $data['Revision']['event'] = 'Create';
            foreach ($Model->data[$Model->alias] as $field => $value) {
                if ($field == 'created' || $field == 'modified')
                    continue;
                $data['Field'][] = array('name' => $field, 'new_value' => $value);
            }
        } else {
            $data['Revision']['event'] = 'Update';
            if (isset($this->_data)) {
                foreach ($Model->data[$Model->alias] as $field => $newValue) {
                    if ($field == 'created' || $field == 'modified')
                        continue;
                    $oldValue = null;
                    if (isset($this->_data[$Model->alias][$field])) {
                        $oldValue = $this->_data[$Model->alias][$field];
                    }
                    if ($oldValue != null && $newValue !== $oldValue) {
                        $data['Field'][] = array(
                            'name' => $field,
                            'old_value' => $oldValue,
                            'new_value' => $newValue
                        );
                    }
                }
                unset($this->_data);
            }
        }

        unset($Revision->Field->validate['revision_id']);
        $Revision->saveAll($data);
    }

    /**
     * Called before a model's afterDelete
     * 
     * @param object $Model The current model
     */
    function afterDelete(&$Model) {
        parent::afterDelete($Model);

        App::import('Component', 'Session');
        $Session = new SessionComponent();
        $user = $Session->read($this->settings[$Model->alias]['user']);

        App::import('Model', 'Revision');
        $Revision = new Revision();
        $data = array(
            'Revision' => array(
                'user_id' => $user['id'],
                'event' => 'Delete',
                'model' => $Model->alias,
                'foreign_key' => $Model->id
            )
        );
        $Revision->save($data);
    }

    /**
     * Binds the Revision model to the current model
     * 
     * @param object $Model The current model
     */
    function bindRevision(&$Model) {
        $params = array(
            'hasMany' => array(
                'Revision' => array(
                    'className' => 'Revision',
                    'foreignKey' => 'foreign_key',
                    'conditions' => "Revision.model = '{$Model->alias}'",
                ),
            ),
        );
        $Model->bindModel($params);
    }

}

?>
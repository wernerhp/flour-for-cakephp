<?php

define('OFFSET', 268435456); // half of the earth circumference in pixels at zoom level 21
define('RADIUS', 85445659.4471); /* $offset / pi() */

//define('OFFSET', 134217728); // half of the earth circumference in pixels at zoom level 21
//define('RADIUS', 42722829.7235); /* $offset / pi() */

class ClusterableBehavior extends ModelBehavior {

    var $minZoom = 2;
    var $maxZoom = 20;
    var $zoomIncrement = 2;
    var $distance = 256;

    function setup(&$Model, $settings) {
        if (!isset($this->settings[$Model->alias])) {
            $this->settings[$Model->alias] = array(
                'option1_key' => 'option1_default_value',
                'option2_key' => 'option2_default_value',
                'option3_key' => 'option3_default_value',
            );
        }
        $this->settings[$Model->alias] = array_merge(
                $this->settings[$Model->alias], (array) $settings);

        App::import('Model', 'Pieterson.Cluster');
        $this->Cluster = new Cluster();
    }

    public function beforeDelete(&$model, $cascade = true) {
        parent::beforeDelete($model, $cascade);
    }

    public function beforeFind(&$model, $query) {
        parent::beforeFind($model, $query);
    }

    public function beforeSave(&$model) {
        parent::beforeSave($model);

        debug('beforeSave');
//        debug($model->data);
//        $points = $this->Cluster->find('all');
//        debug($points);
        return false;

//        $clusters = array();
//        for ($zoom = $this->minZoom; $zoom <= $this->maxZoom; $zoom++) {
//            $clusters[$zoom] = $this->cluster($points, $this->distance, $zoom);
//        }
//        debug($clusters);
//        $cluster = array(
//            'Cluster' => array(
//                'model' => $model->name,
//                'zoom_level' => '',
//                'size' => '',
//                'longitude' => '',
//                'latitude' => '',
//            ),
//        );
//        $this->Cluster->save($cluster);
    }

    /*
      public function beforeValidate(&$model) {
      parent::beforeValidate($model);
      }

      function findClusters(&$Model, $zoom, $bottomLeft, $topRight, $size=1) {

      $clusters = array();
      $points = array();

      // Get all clusters for the bounding box and zoom level
      $query['conditions'] = array(
      'Cluster.model' => $Model->name,
      'Cluster.zoom_level' => $zoom,
      'Cluster.size >' => $size,
      'Cluster.longitude BETWEEN ? AND ?' => array($bottomLeft['longitude'], $topRight['longitude']),
      'Cluster.latitude BETWEEN ? AND ?' => array($bottomLeft['latitude'], $topRight['latitude']),
      );
      $this->Cluster->unbindModel(array('belongsTo' => array('*'), 'hasOne' => array('*'), 'hasMany' => array('ClustersPoint'), 'hasAndBelongsToMany' => array('*')), false);
      $clusters = $this->Cluster->find('all', $query);

      // Get all points for the bounding box and zoom level
      $dbo = $this->Cluster->getDataSource();
      $subQuery = $dbo->buildStatement(
      array(
      'fields' => array('Cluster.id'),
      'table' => $dbo->fullTableName($this->Cluster),
      'alias' => 'Cluster',
      'limit' => null,
      'offset' => null,
      'joins' => array(),
      'conditions' => array(
      'Cluster.model' => $Model->name,
      'Cluster.zoom_level' => $zoom,
      'Cluster.size >' => $size,
      ),
      'order' => null,
      'group' => null
      ), $this->Cluster
      );
      $subQuery = $dbo->buildStatement(
      array(
      'fields' => array('ClustersPoint.foreign_key'),
      'table' => $dbo->fullTableName($this->Cluster->ClustersPoint),
      'alias' => 'ClustersPoint',
      'limit' => null,
      'offset' => null,
      'joins' => array(),
      'conditions' => array(
      ' `ClustersPoint`.`cluster_id` IN (' . $subQuery . ') ',
      ' `ClustersPoint`.`zoom_level` = '.$zoom,
      ' `ClustersPoint`.`model`' => $Model->name,
      ),
      'order' => null,
      'group' => null
      ), $this->Cluster->ClustersPoint
      );
      $subQuery = $dbo->buildStatement(
      array(
      'fields' => array($Model->name . '.*'),
      'table' => $dbo->fullTableName($Model),
      'alias' => $Model->name,
      'limit' => null,
      'offset' => null,
      'joins' => array(),
      'conditions' => array(
      $Model->name . '.longitude BETWEEN ? AND ?' => array($bottomLeft['longitude'], $topRight['longitude']),
      $Model->name . '.latitude BETWEEN ? AND ?' => array($bottomLeft['latitude'], $topRight['latitude']),
      ' `' . $Model->name . '`.`id` NOT IN (' . $subQuery . ') '
      ),
      'order' => null,
      'group' => null
      ), $this->Cluster->ClustersPoint
      );

      $points = $Model->query($subQuery);

      $clusters = Set::classicExtract($clusters, '{n}.Cluster');
      $points = Set::classicExtract($points, '{n}.' . $Model->name);
      $results = array(
      'Cluster' => $clusters,
      $Model->name => $points,
      );
      return $results;
      }
     */

    function findClusters(&$Model, $zoom, $bottomLeft, $topRight, $size = 1) {

        // if(is_odd($zoom)) {
        if ($zoom & 1) {
            $zoom++;
            if ($zoom < 2)
                $zoom = 2;
            if ($zoom > 20)
                $zoom = 20;
        }

        $clusters = array();
        $points = array();

        /* Get all clusters for the bounding box and zoom level */
        $query['conditions'] = array(
            'Cluster.model' => $Model->name,
            'Cluster.zoom_level' => $zoom,
            'Cluster.size >=' => $size,
            'Cluster.longitude BETWEEN ? AND ?' => array($bottomLeft['longitude'], $topRight['longitude']),
            'Cluster.latitude BETWEEN ? AND ?' => array($bottomLeft['latitude'], $topRight['latitude']),
        );
        $this->Cluster->unbindModel(array('belongsTo' => array('*'), 'hasOne' => array('*'), 'hasMany' => array('ClustersPoint'), 'hasAndBelongsToMany' => array('*')), false);
        $clusters = $this->Cluster->find('all', $query);

        /* Get all points for the bounding box and zoom level */
        $dbo = $this->Cluster->getDataSource();
        $query = array(
            'fields' => array($Model->name . '.*'),
            'joins' => array(
                array(
                    'table' => 'clusters_points',
                    'alias' => 'ClustersPoint',
                    'type' => 'LEFT',
                    'conditions' => array(// Joining on multiple conditions speeds things up
                        'Cluster.id = ClustersPoint.cluster_id',
                        'Cluster.zoom_level = ClustersPoint.zoom_level',
                        'Cluster.model = ClustersPoint.model',
                    ),
                ),
                array(
                    'table' => $dbo->fullTableName($Model),
                    'alias' => $Model->name,
                    'type' => 'LEFT',
                    'conditions' => array(// Joining on multiple conditions speeds things up
                        'ClustersPoint.foreign_key = ' . $Model->name . '.id',
                        'ClustersPoint.model = "' . $Model->name . '"',
                    ),
                ),
            ),
            'conditions' => array(
                'Cluster.model' => $Model->name,
                'Cluster.zoom_level' => $zoom,
                'Cluster.size <' => $size,
                'Cluster.longitude BETWEEN ? AND ?' => array($bottomLeft['longitude'], $topRight['longitude']),
                'Cluster.latitude BETWEEN ? AND ?' => array($bottomLeft['latitude'], $topRight['latitude']),
            )
        );

        $points = $this->Cluster->find('all', $query);

        $clusters = Set::classicExtract($clusters, '{n}.Cluster');
        $points = Set::classicExtract($points, '{n}.' . $Model->name);
        $results = array(
            'Cluster' => $clusters,
            $Model->name => $points,
        );

        return $results;
    }

    /**
     * This function loops through all the data for the given model and builds
     * up clusters.  This needs to be done only once at the beginning or if
     * clusters get corrupted for some reason.  Using Clusterable will
     * automatically add new points to clusters.
     */
    function buildClusters(&$Model, $zoom_level = null, $delete = false) {

        ignore_user_abort(true);
        $startTime = time();
        print_r(date('Y-m-d H:i:s') . " buildClusters started<br/>");
        $this->Cluster->flush();

        if ($zoom_level != null) {
            $this->minZoom = $this->maxZoom = $zoom_level;
        }
        set_time_limit(0);
        ini_set("memory_limit", "512M");

        $this->Cluster->unbindModel(array('belongsTo' => array('*'), 'hasOne' => array('*'), 'hasMany' => array('*'), 'hasAndBelongsToMany' => array('*')));
        $this->Cluster->ClustersPoint->unbindModel(array('belongsTo' => array('*'), 'hasOne' => array('*'), 'hasMany' => array('*'), 'hasAndBelongsToMany' => array('*')));
        if ($delete) {
            print_r(date('Y-m-d H:i:s') . " Deleting ClusterPoints for " . $Model->name . " zoom level " . $zoom_level . "<br/>");
            $this->Cluster->flush();
            $this->Cluster->ClustersPoint->deleteAll(array('ClustersPoint.model' => $Model->name, 'ClustersPoint.zoom_level' => $zoom_level));
            print_r(date('Y-m-d H:i:s') . " Deleting Clusters for " . $Model->name . " zoom level " . $zoom_level . "<br/>");
            $this->Cluster->flush();
            $this->Cluster->deleteAll(array('Cluster.model' => $Model->name, 'Cluster.zoom_level' => $zoom_level));
        }
        print_r(date('Y-m-d H:i:s') . " Getting all points<br/>");
        $this->Cluster->flush();
        $Model->unbindModel(array('belongsTo' => array('*'), 'hasOne' => array('*'), 'hasMany' => array('*'), 'hasAndBelongsToMany' => array('*')));
        $points = $Model->find('all');
        foreach ($points as &$point) {
            $tmp = array();
            $tmp = $point[$Model->name];
            $point = $tmp;
        }

        $clusters = array();

        for ($zoom = $this->minZoom; $zoom <= $this->maxZoom; $zoom+=$this->zoomIncrement) {
            print_r(date('Y-m-d H:i:s') . " Calculating clusters for zoom level " . $zoom . "<br/>");
            $this->Cluster->flush();
            if ($this->cluster($points, $this->distance, $zoom, $Model->name)) {
                print_r(date('Y-m-d H:i:s') . " Zoom level " . $zoom . " completed.<br/>");
            } else {
                print_r(date('Y-m-d H:i:s') . " Error on zoom level " . $zoom . ".<br/>");
            }
            $this->Cluster->flush();
        }
        print_r(date('Y-m-d H:i:s') . " buildClusters finished<br/>");
        $this->Cluster->flush();

        $endTime = time();
        $time = date('H:i:s', ($endTime - $startTime));

        print_r("Total job time: " . $time . "<br/>");
        $this->Cluster->flush();

        die();
    }

    function haversineDistance($lat1, $lon1, $lat2, $lon2) {
        $latd = deg2rad($lat2 - $lat1);
        $lond = deg2rad($lon2 - $lon1);
        $a = sin($latd / 2) * sin($latd / 2) +
                cos(deg2rad($lat1)) * cos(deg2rad($lat2)) *
                sin($lond / 2) * sin($lond / 2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
        return 6371.0 * $c;
    }

    function lonToX($lon) {
        return round(OFFSET + RADIUS * $lon * pi() / 180);
    }

    function latToY($lat) {
        return round(OFFSET - RADIUS *
                        log((1 + sin($lat * pi() / 180)) /
                                (1 - sin($lat * pi() / 180))) / 2);
    }

    function pixelDistance($lat1, $lon1, $lat2, $lon2, $zoom) {
        $x1 = $this->lonToX($lon1);
        $y1 = $this->latToY($lat1);

        $x2 = $this->lonToX($lon2);
        $y2 = $this->latToY($lat2);

        return sqrt(pow(($x1 - $x2), 2) + pow(($y1 - $y2), 2)) >> (21 - $zoom);
    }

    function cluster($markers, $distance, $zoom, $model = 'Point') {
        $success = true;
        $numOfPoints = 0;
        $totalPoints = 0;
        $cluster_points = array(
            'ClustersPoint' => array(),
        );

        /* Loop until all markers have been compared. */
        while (count($markers)) {
            /* Get a marker */
            $marker = array_pop($markers);

            /* Create cluster */
            $clusterSize = 0;
            $cluster = array(
                'zoom_level' => $zoom,
                'model' => $model,
                'size' => $clusterSize,
                'longitude' => $marker['longitude'],
                'latitude' => $marker['latitude'],
            );
            $this->Cluster->create();
            $this->Cluster->save($cluster, false);

            /* Compare against all markers which are left. */
            foreach ($markers as $key => $target) {

                $pixels = $this->pixelDistance($marker['latitude'], $marker['longitude'], $target['latitude'], $target['longitude'], $zoom);
                /* If two markers are closer than given distance remove target marker from array and add it to cluster. */
                if ($distance > $pixels) {
                    unset($markers[$key]);  // Remove target marker from array
                    $cluster_point = array();  // Create a cluster_point relationship
                    $cluster_point['cluster_id'] = $this->Cluster->id;
                    $cluster_point['model'] = $model;
                    $cluster_point['foreign_key'] = $target['id'];
                    $cluster_point['zoom_level'] = $zoom;
                    $cluster_points['ClustersPoint'][] = $cluster_point;  // Add it to cluster
                }

                if (($numOfPoints = count($cluster_points['ClustersPoint'])) >= 5000) {
                    $clusterSize += $numOfPoints;

                    /* Save custers_points relationships */
                    $this->Cluster->ClustersPoint->create();
                    if ($this->Cluster->ClustersPoint->saveAll($cluster_points['ClustersPoint'], array('validate' => false))) {
                        $cluster_points = array(
                            'ClustersPoint' => array(),
                        );

                        // print_r(date('Y-m-d H:i:s') . " Saving " . $numOfPoints . " points.  Cluster " . $this->Cluster->id . " size: " . $clusterSize . ". ");
                        // $this->Cluster->flush();

                        /* Update cluster size */
                        if ($this->Cluster->saveField('size', $clusterSize, false)) {
                            // print_r(date('Y-m-d H:i:s') . " Total so far " . $totalPoints . ".<br/>");
                            // $this->Cluster->flush();
                        } else {
                            $success = false;
                        }
                    } else {
                        $success = false;
                        $this->log("ClusterPoint NOT saved", 'clusterable');
                        $this->log($cluster_point, 'clusterable');
                    }
                }
            }

            /* If a marker has been added to cluster, add also the one we were comparing to and remove the original from array. */
            $cluster_point = array();  // Create a cluster_point relationship
            $cluster_point['cluster_id'] = $this->Cluster->id;
            $cluster_point['model'] = $model;
            $cluster_point['foreign_key'] = $marker['id'];
            $cluster_point['zoom_level'] = $zoom;
            $cluster_points['ClustersPoint'][] = $cluster_point;  // Add it to cluster
            // if(!count($markers)) {
            if (($numOfPoints = count($cluster_points['ClustersPoint'])) > 0) {
                $clusterSize += $numOfPoints;

                print_r(date('Y-m-d H:i:s') . " Saving " . $numOfPoints . " points.  Cluster " . $this->Cluster->id . " size: " . $clusterSize . ". ");
                $this->Cluster->flush();

                /* Save custers_points relationships */
                $this->Cluster->ClustersPoint->create();
                if ($this->Cluster->ClustersPoint->saveAll($cluster_points['ClustersPoint'], array('validate' => false))) {
                    $cluster_points = array(
                        'ClustersPoint' => array(),
                    );

                    /* Update cluster size */
                    if ($this->Cluster->saveField('size', $clusterSize, false)) {
                        $totalPoints += $clusterSize;
                        print_r(date('Y-m-d H:i:s') . " Total: " . $totalPoints . ".<br/>");
                        $this->Cluster->flush();
                    } else {
                        $success = false;
                    }
                } else {
                    $success = false;
                    $this->log("ClusterPoint NOT saved", 'clusterable');
                    $this->log($cluster_point, 'clusterable');
                }
            }
            // }
        }
        return $success;
    }

}

?>

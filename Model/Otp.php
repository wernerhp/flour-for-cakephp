<?php

/**
 * One-Time Password Model
 *
 * A database driven One-Time Password solution for CakePHP.
 * If you have problems, view the validationErrors and authenticationErrors
 *
 * @author  Werner Pieterson <wernerhp@gmail.com>
 */
class Otp extends FlourAppModel {

    var $name = 'Otp';

    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);

        $this->validate = array(
            'otu' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => __('OTU required'),
                    'allowEmpty' => false,
                    'required' => true,
                    'last' => true, // Stop validation after this rule
                ),
            ),
            'otp' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => __('OTP required'),
                    'allowEmpty' => false,
                    'required' => true,
                    'last' => true, // Stop validation after this rule
                ),
                'minLength' => array(
                    'rule' => array('minLength', 4),
                    'message' => __('Minimum otp length is 4 characters'),
                    'allowEmpty' => false,
                    'required' => true,
                    'last' => true, // Stop validation after this rule
                ),
            ),
            'expires' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => __('Expiration date required'),
                    'allowEmpty' => false,
                    'required' => true,
                    'last' => true, // Stop validation after this rule
                ),
                'date' => array(
                    'rule' => '/^\d{4}-\d{2}-\d{2} \d{1,2}:\d{2}:\d{2}$/',
                    'message' => __('Invalid date format.  Expected YYYY-MM-DD HH:MM:SS'),
                    'allowEmpty' => false,
                    'required' => true,
                    'last' => true, // Stop validation after this rule
                ),
            ),
        );
    }

    /**
     * Time to live
     * 
     * How long (in seconds) the OTP is valid.
     *
     * @var integer
     * @access public
     */
    var $ttl = 172800; // two (2) days
    /**
     * Size
     *
     * The size of the generated OTP.
     *
     * @var integer
     * @access public
     */
    var $size = 40;

    /**
     * One-Time Username
     *
     * The unique username associated with a OTP
     *
     * @var string
     * @access public
     */
    var $otu = null;

    /**
     * One-Time Password
     *
     * The unique password associated with a OTU
     *
     * @var string
     * @access public
     */
    var $otp = null;

    /**
     * action
     *
     * The action to perform after successful authentication
     *
     * @var string
     * @access public
     */
    var $action = null;

    /**
     * attempts_allowed
     *
     * The number of attempts before OTP expires
     *
     * @var integer
     * @access public
     */
    var $attempts_allowed = 3;

    /**
     * debug
     *
     * Enables debug mode, which doesn't update failed attempts
     * or expire tokens.  Must be set to false when in production
     *
     * @var boolean
     * @access public
     */
    var $debug = false;

    /**
     * List of authentication errors.
     *
     * @var array
     * @access public
     */
    var $authenticationErrors = array();

    /**
     * Generates a One-Time Password and saves it to the database
     * @param mixed $options List of options to use when generating OTP
     * Options: ttl, size, otu, otp, action, attempts_allowed
     * @return mixed array with One-Time Password or false on error
     * @access public
     */
    function generate($options = array()) {

        App::uses('AuthComponent', 'Controller/Component');

        if (array_key_exists('ttl', $options)) {
            $this->ttl = $options['ttl'];
        }
        if (array_key_exists('size', $options)) {
            $this->size = $options['size'];
        }
        if (array_key_exists('otu', $options)) {
            $this->otu = $options['otu'];
        } else {
            $this->otu = trim(substr(AuthComponent::password(rand() . microtime(true)), 0, $this->size));
        }
        if (array_key_exists('action', $options)) {
            $this->action = $options['action'];
        }
        if (array_key_exists('attempts_allowed', $options)) {
            $this->attempts_allowed = $options['attempts_allowed'];
        }

        $data['otu'] = $this->otu;
        $data['otp'] = trim(substr(AuthComponent::password($this->otu . microtime(true)), 0, $this->size));
        $data['action'] = $this->action;
        $data['attempts_allowed'] = $this->attempts_allowed;
        $data['expires'] = date('Y-m-d H:i:s', microtime(true) + $this->ttl);

        if (($this->data = $this->save($data))) {
            return $this->data;
        } else {
            return false;
        }
    }

    /**
     * Authenticates a One-Time Password
     * @param array $options List of options to authenticate
     *
     * Supported options:
     * - otu Username as a string value
     * - otp Password as a string value or an array of passwords
     *
     * @return mixed true on success or false on error or an array of actions
     * @access public
     */
    function authenticate($options = array()) {

        if (array_key_exists('otu', $options)) {
            $this->otu = $options['otu'];
        }
        if (array_key_exists('otp', $options)) {
            $this->otp = $options['otp'];
        }

        // Get current dateTime
        $now = date('Y-m-d H:i:s', microtime(true));

        // Get all passed OTPs that are unused
        $this->data = $this->find('all', array('conditions' => array(
                'otu' => $this->otu,
                'otp' => $this->otp,
                'attempts < attempts_allowed',
                'Otp.created = Otp.modified',
                'expires >' => $now,
        )));

        if (!$this->debug) {
            // Update failed attempts
            $this->updateAll(
                    array(
                'Otp.attempts' => '`Otp`.`attempts` + 1',
                'Otp.modified' => 'IF(`Otp`.`attempts` >= `Otp`.`attempts_allowed`, "' . $now . '", `Otp`.`modified`)',
                    ), array(
                'Otp.otu' => $this->otu,
                'Otp.created = Otp.modified',
                'Otp.expires >' => $now,
                    )
            );
        }
        // Check that all passed OTPs are valid
        if (count($this->otp) != count($this->data) || empty($this->otp)) {
            $this->authenticationErrors = __('One or more of the OTPs provided were invalid.');
            return false;
        }

        foreach ($this->data as $data) {
            // Build array of IDs to expire
            $otps[] = $data['Otp']['id'];

            // Build array of actions to return
            if (!empty($data['Otp']['action'])) {
                $actions[] = $data['Otp']['action'];
            }
        }

        // Expire OTPs
        if (!$this->debug) {
            if ($this->updateAll(
                            array(
                        'Otp.modified' => '"' . $now . '"',
                            ), array('Otp.id' => $otps)
                    )) {
                if (!empty($actions)) {
                    return $actions;
                }
                return true;
            } else {
                $this->authenticationErrors = __('There was an error while attempting to expire the OTPs.');
                return false;
            }
        } else {
            if (!empty($actions)) {
                return $actions;
            }
            return true;
        }
    }
}

?>
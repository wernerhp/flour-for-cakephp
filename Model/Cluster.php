<?php

class Cluster extends FlourAppModel {

    var $name = 'Cluster';
    var $useTable = 'clusters';
    var $hasMany = array(
        'ClustersPoint'
    );

    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);

        // TODO: Create table by calling shell to run config/schema/cluster.php if it dosn't exist
        // cake schema create Flour.Cluster
        // If table for Model Log doesn't exist, you get the following error
        // Fatal error: Class 'Controller' not found in ...
    }

}

?>

<?php

class ClustersPoint extends FlourAppModel {

    var $name = 'ClustersPoint';
    var $useTable = 'clusters_points';
    var $belongsTo = array(
        'Cluster'
    );

}

?>

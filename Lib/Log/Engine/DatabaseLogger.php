<?php

/**
 * DatabaseLogger
 * 
 * A logging library for saving logs to a database.
 * 
 * 1.  Create the `db_log` database table using the Cake Console.
 *     .\Console\cake schema create Flour.DbLog
 * 2.  Configure the DatabaseLogger engine in bootstrap.php
 *     CakeLog::config('DatabaseLogger', array(
 *         'engine' => 'Flour.DatabaseLogger',
 *     ));
 * 
 * @author  Werner Pieterson <wernerhp@gmail.com>
 */
App::uses('CakeLogInterface', 'Log');

class DatabaseLogger implements CakeLogInterface {

    public function __construct($options = array()) {
        // TODO: Add options for Model and useTable
        App::import('Model', 'Flour.DbLog');
        $this->DbLog = new DbLog();
    }

    public function write($type, $message) {
        try {
            $this->DbLog->create();
            $log = array();
            $log['type'] = ucfirst($type);
            $log['time'] = date('Y-m-d H:i:s');
            $log['message'] = $message;
            return $this->DbLog->save($log);
        } catch (Exception $exc) {
            trigger_error(__d('cake_dev', 'Please create \'db_log\' table in database to use DatabaseLogger'), E_USER_NOTICE);
        }

        return false;
    }
}

?>
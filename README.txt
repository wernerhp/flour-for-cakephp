OTP (One-Time Password)
======================
An OTP Model for generating and authenticating One-Time Passwords.

1.  Create the `otp` database table using the Cake Console.
    .\Console\cake schema create Flour.Otp
2.  Load the Flour.Otp model
    $this->loadModel('Flour.Otp');
3.  Call the generate function to generate an OTP.  View Otp.php source for 
    available options.
    $otp = $this->Otp->generate(array('otu'=>'One-Time Username'));
4.  Call the authenticate function to authenticate a OTP.
    $otp = $this->Otp->authenticate(array('otu'=>'One-Time Username', 'otp'=>'3b66631b'));

jValidate
=========
jValidate allows you to easily add AJAX validation to forms inputs using jQuery.

1.  Add the following to your app's bootstrap.php
    CakePlugin::load('Flour', array('routes' => true)); 
2.  Add the RequestHandler Component to your AppController.
    public $components = array('RequestHandler');
3.  Add the Flour.jValidate Helper to your AppController.
    public $helpers = array('Flour.jValidate');
4.  Add the jValidate class to any input you want to use validate using jValidate.
    <input type='text' name='example' class='jValidate' />
5.  All done.

DbLogger
========
A logging library for saving logs to a database.

1.  Create the `db_log` database table using the Cake Console.
    .\Console\cake schema create Flour.DbLog
2.  Configure the DatabaseLogger engine in bootstrap.php
    CakeLog::config('DatabaseLogger', array(
        'engine' => 'Flour.DatabaseLogger',
    ));
3.  View db_logs at http://localhost/yourapp/flour/db_logs

Auditable Behavior
==================
A behavior for keeping an audit trail / version history with an option to revert
to a previous point in time.

Clusterable Behavior
====================
A behavior for clustering large sets of data for plotting on a map.

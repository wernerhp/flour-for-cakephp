<?php echo $this->Html->script('permissions', array('inline' => false)); ?>
<div class="permissions index">
    <h1><?php echo __('Permissions'); ?></h1>
    <div class="actions">
        <ul>
            <li><?php echo $this->Html->link(__('Add missing'), array('action' => 'add_missing'), array('title' => 'Add missing permissions', 'class' => 'button add')); ?></li>
            <li><?php echo $this->Html->link(__('Clear'), array('action' => 'clear'), array('title' => 'Clear permissions list', 'class' => 'button delete'), __('Are you sure you want to clear the permissions list?')); ?></li>
        </ul>
        <div class="clear"></div>
    </div>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th>&nbsp;</th>
                <?php foreach ($groups as $group): ?>
                    <th><?php echo $group['Group']['name']; ?></th>
                <?php endforeach; ?>
                <th class="actions"><?php echo __('Actions'); ?></th>
            </tr>            
        </thead>
        <tfoot>
            <tr class="header">
                <td>&nbsp;</td>
                <?php foreach ($groups as $group): ?>
                    <td><?php echo $group['Group']['name']; ?></td>
                <?php endforeach; ?>
                <td class="actions">&nbsp;</td>
            </tr>
        </tfoot>
        <tbody>
            <?php foreach ($objects as $id => $object): ?>
                <tr>
                    <td><?php echo $object['name']; ?></td>
                    <?php foreach ($groups as $group): ?>
                        <td>
                            <?php
                            switch ($object['level']) {
                                case 0: $class = 'root';
                                    break;
                                case 1: $class = 'controller';
                                    break;
                                case 2: $class = 'action';
                                    break;
                                default: $class = '';
                                    break;
                            }

                            if ($permissions[$group['Aro']['id']][$id]) {
                                $image = $this->Html->image('icons/ball_green.png', array('alt' => 'Yes', 'width' => 32, 'height' => 32));
                                $url = array(
                                    'action' => 'deny',
                                    'model' => $group['Aro']['model'],
                                    'foreign_key' => $group['Aro']['foreign_key'],
                                    'aco' => base64_encode($object['path'])
                                );
                                $options = array('class' => $class, 'title' => 'Deny', 'escape' => false);
                                echo $this->Html->link($image, $url, $options);
                            } else {
                                $image = $this->Html->image('icons/ball_red.png', array('alt' => 'No', 'width' => 32, 'height' => 32));
                                $url = array(
                                    'action' => 'allow',
                                    'model' => $group['Aro']['model'],
                                    'foreign_key' => $group['Aro']['foreign_key'],
                                    'aco' => base64_encode($object['path'])
                                );
                                $options = array('class' => $class, 'title' => 'Allow', 'escape' => false);
                                echo $this->Html->link($image, $url, $options);
                            }
                            ?>
                        </td>
                <?php endforeach; ?>
                    <td class="actions"><?php echo $this->Html->link(__('Remove'), array('action' => 'delete', $id), array('title' => 'Remove permission', 'class' => 'button delete'), __('Are you sure you want to remove the selected permission?')); ?></td>
                </tr>
<?php endforeach; ?>
        </tbody>
    </table>    
</div>

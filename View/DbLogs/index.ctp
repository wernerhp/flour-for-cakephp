<div class="dbLogs">
    <h2><?php echo __('Database Logs'); ?></h2>
    <?php echo $this->Html->link(__('Clear Logs'), array('action' => 'truncate'), array('class' => 'button dblog-clear'), sprintf(__('Are you sure you want to clear all logs?'))); ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th class="dblog-type"><?php echo $this->Paginator->sort('id'); ?></th>
            <th class="dblog-type"><?php echo $this->Paginator->sort('type'); ?></th>
            <th class="dblog-time"><?php echo $this->Paginator->sort('time'); ?></th>
            <th class="dblog-message"><?php echo $this->Paginator->sort('message'); ?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($dbLogs as $dbLog):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr<?php echo $class; ?>>
                <td><?php echo $this->Html->link($dbLog['DbLog']['id'], array('action' => 'view', $dbLog['DbLog']['id'])); ?>&nbsp;</td>
                <td><?php echo $dbLog['DbLog']['type']; ?>&nbsp;</td>
                <td><?php echo $dbLog['DbLog']['time']; ?>&nbsp;</td>
                <td><?php echo $dbLog['DbLog']['message']; ?>&nbsp;</td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%')
        ));
        ?>	</p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('previous'), array(), null, array('class' => 'disabled')); ?>
        | 	<?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('next') . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>
</div>
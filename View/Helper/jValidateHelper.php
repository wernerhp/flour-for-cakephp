<?php

App::uses('AppHelper', 'View/Helper');

/**
 * JValidateHelper loads all the libaries that are required.
 *
 * @author Werner Pieterson <wernerhp@gmail.com>
 */
class JValidateHelper extends AppHelper {

    private $pluginName = 'Flour';
    public $helpers = array('Html', 'Js');

    public function __construct(View $View, $settings = array()) {
        parent::__construct($View, $settings);

        /* Load jQuery from Google's CDN */
        echo $this->Html->script('http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js', array('inline' => false, 'once' => true));
        /* Load jQuery from Flour's webroot if the CDN is down */
        echo $this->Html->scriptBlock('
            !window.jQuery && document.write("<script src=\'' . Router::url("/flour/js/jquery-1.10.1.min.js") . '\'><\/script>");
        ', array('safe' => true, 'inline' => false));
        /* Define the Flour base path */
        echo $this->Html->scriptBlock('
            jQuery(document).ready(function($) {
                $FLOUR_BASE_PATH = "' . $this->Html->url("/", true) . '"
            });
            ', array('safe' => true, 'inline' => false));

//        $this->Js->buffer('$APP = "' . $this->Html->url('/', true) . '"');
//        echo $this->Js->writeBuffer(array('onDomReady' => false)); // Write cached scripts
        echo $this->Html->script($this->pluginName . '.json2.js', array('inline' => false, 'once' => true));
        echo $this->Html->script($this->pluginName . '.form2js/form2js.js', array('inline' => false, 'once' => true));
        echo $this->Html->script($this->pluginName . '.form2js/jquery.toObject', array('inline' => false, 'once' => true));
        echo $this->Html->script($this->pluginName . '.jValidate.js', array('inline' => false, 'once' => true));
    }
}
?>

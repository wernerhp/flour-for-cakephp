jQuery(document).ready(function($) {
    $('input.jValidate').change(jValidate);
    $('input.jValidate').blur(jValidate);
    $('select.jValidate').change(jValidate);
    $('select.jValidate').blur(jValidate);

    function jValidate(event) {
        currentElement = $(this);
        var jqXHR = jQuery.ajax({
            type: "POST",
            contentType: "application/json",
            dataType: "json",
            url: $FLOUR_BASE_PATH + "flour/jValidate/jValidate.json",
            data: JSON.stringify(currentElement.toObject({
                "skipEmpty": false
            }).data)
        }).done(function(data, textStatus, jqXHR) {
            currentElement.parent().removeClass('error');
            currentElement.parent().find('.error-message').remove();
            if (data.length) {
                currentElement.parent().addClass('error');
                currentElement.after('<div class="error-message">' + data + '</div>');
            }
        });
    }
});